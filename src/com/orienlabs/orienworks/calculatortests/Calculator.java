package com.orienlabs.orienworks.calculatortests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.orienlabs.orienworks.AndroidDriver;
import com.orienlabs.orienworks.BY;

public class Calculator
{
    public static String androidSdkLocation= "/Volumes/D/android-sdk-mac/sdk";
    public static String deviceNameOrSerial= "192.168.56.101:5555";
    public static AndroidDriver driver;

    @BeforeTest
    public void beforeTest() throws Exception
    {
        // Specify androidSdk by argument instead of ANDROID_HOME env variable, along with specific deviceSerialNo
        // driver= new AndroidDriver(androidSdkLocation, deviceNameOrSerial);

        // Use androidSdk from ANDROID_HOME env variable, pick up any device connected to ADB
        driver= new AndroidDriver();

        // Install an application
        // driver.installApplication(appInstallerLocation);
    }

    @Test
    public void testAddition() throws Exception
    {
        driver.launchApplication("com.android.calculator2/.Calculator", false);

        // Empty exising texts
        String previousResult= driver.findElement("xpath", "//EditText | //CalculatorEditText").getText();
        for (int i= 0; i < previousResult.length(); i++)
        {
            driver.findElement(BY.xpath, "//*[@id='del'] | //*[@id='clear']").tap(false);
        }
        driver.findElement(BY.text, "7").tap(false);
        driver.findElement(BY.text, "8").tap(false);
        driver.findElement(BY.text, "+").tap(false);
        driver.findElement(BY.text, "2").tap(false);
        driver.findElement(BY.text, "=").tap(false);

        driver.useElementCache= false;
        String currentResult= driver.findElement("xpath", "//EditText | //CalculatorEditText").getText();
        Assert.assertEquals(currentResult, "80");
    }

    @AfterTest
    public void afterTest() throws Exception
    {
        System.out.println(driver.getXmlScreenshot());
        System.out.println(driver.getJpegScreenshot());
        driver.shutdown();
    }

}
